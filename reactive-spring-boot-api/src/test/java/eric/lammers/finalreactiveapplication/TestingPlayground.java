package eric.lammers.finalreactiveapplication;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.Disposable;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

/*
 * A playground to get my hands dirty with some of the react-core features
 * Many of the examples from: https://www.baeldung.com/reactor-core
 */
@Log4j2
public class TestingPlayground {

    @Test
    public void collectingElements() {
        List<Integer> elements = new ArrayList<>();

        Flux.just(1,2,3,4).log().subscribe(elements::add);

        assertThat(elements).containsExactly(1,2,3,4);
    }

    @Test
    public void subscriberInterface() {
        List<Integer> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4).subscribe(new Subscriber<Integer>() {
                @Override
                public void onSubscribe(Subscription s) {
                    s.request(Long.MAX_VALUE);
                    log.info("Subscribe to stream");
                }

                @Override
                public void onNext(Integer integer) {
                    elements.add(integer);
                    log.info("Add alement: " + integer);
                }

                @Override
                public void onError(Throwable t) {
                    log.info("Error Occurred");
                }

                @Override
                public void onComplete() {
                    log.info("Completed");
                }
            });

        assertThat(elements).containsExactly(1,2,3,4);
    }

    // The log here will show requests being made multiple times for 2 element
    // Unlike the above code with makes a request(unbounded) request
    @Test
    public void backpressure() {
        List<Integer> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
            .log()
            .subscribe(new Subscriber<Integer>() {
                private Subscription s;
                int onNextAmount;

                @Override
                public void onSubscribe(Subscription s) {
                    this.s = s;
                    s.request(2);
                }

                @Override
                public void onNext(Integer integer) {
                    elements.add(integer);
                    onNextAmount++;
                    if (onNextAmount % 2 == 0) {
                        s.request(2);
                    }
                }

                @Override
                public void onError(Throwable t) {}

                @Override
                public void onComplete() {}
            });

        assertThat(elements).containsExactly(1,2,3,4);
    }

    @Test
    public void mapping() {
        List<Integer> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
            .log()
            .map(i -> i * 2)
            .subscribe(elements::add);

        assertThat(elements).containsExactly(2,4,6,8);
    }

    @Test
    public void combiningStreams() {
        List<String> elements = new ArrayList<>();

        Flux.just(1, 2, 3, 4)
            .log()
            .map(i -> i * 2)
            .zipWith(
                Flux.range(0, Integer.MAX_VALUE).log(),
                (one, two) -> String.format("First Flux: %d, Second Flux: %d", one, two)
            )
            .subscribe(elements::add);

        assertThat(elements).containsExactly(
            "First Flux: 2, Second Flux: 0",
            "First Flux: 4, Second Flux: 1",
            "First Flux: 6, Second Flux: 2",
            "First Flux: 8, Second Flux: 3");
    }

    @Test
    public void connectableFlux() throws Exception {
        Flux<Integer> source = Flux.range(1, 3)
            .doOnSubscribe(s -> System.out.println("subscribed to source"));

        ConnectableFlux<Integer> publish = source.publish();

        publish.subscribe(System.out::println, e -> {}, () -> {});
        publish.subscribe(System.out::println, e -> {}, () -> {});

        System.out.println("done subscribing");
        Thread.sleep(500);
        System.out.println("will now connect");

        publish.connect();
    }

    // TODO: Determine how to stop a connected publisher
    @Test
    public void hotStreams() throws Exception {

        ConnectableFlux<Object> publish = Flux.create(fluxSink -> {
            int count = 0;
            while(count < 10000) {
                fluxSink.next(System.currentTimeMillis());
                count++;
            }
        })
            .publish();

        publish.subscribe(System.out::println, e -> {}, () -> {});
        publish.subscribe(System.out::println, e -> {}, () -> {});

        System.out.println("done subscribing");
        Thread.sleep(500);
        System.out.println("will now connect");

        Disposable disposable = publish.connect();

        disposable.dispose();
    }
}
