package eric.lammers.finalreactiveapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// https://developer.okta.com/blog/2018/09/24/reactive-apis-with-spring-webflux
@SpringBootApplication
public class FinalReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalReactiveApplication.class, args);
	}

}
