### Description
This folder contains example requests that can be executed against the API. These requests 
can be sent using the Visual Studio Code plugin [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client).
See their docs for the details.