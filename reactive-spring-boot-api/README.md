### Description
Sample application put together to get some experience with the new reactive features in Spring Boot
2.0. 

The code from this application was created by following along with the tutorial [Reactive APIs with Spring Webflux](https://developer.okta.com/blog/2018/09/24/reactive-apis-with-spring-webflux).

### Running the application
1. Start the mongodb database
    a. Run: `docker-compose up` this starts a single docker container running mongodb
2. Start the application
    a. In Intellij create a spring boot run configuration
    b. Add the VM option `-Dspring.profiles.active=demo`
    c. Press play
    
### Hitting the endpoints
In the docs/request folder a number of example requests have been created. These example requests can be sent using the 
Visual Studio Code plugin [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client).
See their docs for the details.
    
### Key Takeaways 
#### Reactive 
- Unlike java 8 streams which have a pull model reactive is a push model where events are pushed to the subscriber as 
they come in
- Streams end, we pull all the data then get a result, with reactive we could have an infinite stream (for example coming
from an external source)
- **Backpressure** is when a downstream can tell an upstream to send it fere data in order to prevent if from being 
overwhelmed
- Cold stream refers to a stream that is static and of fixed length, hot stream is something that is always running and can be 
subscribed to at any point

##### Spring 5
- You can use the annotation approach for describing the endpoints of the reactive web service, but although it looks like 
spring mvc it is not
- Reactive web applications and functional programming go together nicely
