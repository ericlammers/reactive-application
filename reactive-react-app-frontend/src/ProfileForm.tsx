import React, { useState } from 'react';
import './ProfileForm.scss';

const ProfileForm = () => {
    const [email, setEmail] = useState('');

    const postNewProfile = () => {
        fetch('http://localhost:3000/profiles', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({'email': email})
        });

        setEmail('');
    };

    return (
        <div className="profile-form">
            <h2>Add a Profile</h2>
            <input onChange={event => setEmail(event.target.value)} value={email}/>
            <button onClick={postNewProfile}>Add</button>
        </div>
    )
};

export default ProfileForm;