import React, { Component } from 'react';
import './App.scss';
import ProfileList from './ProfileList';
import ProfileForm from "./ProfileForm";

class App extends Component {
  render() {
    return (
        <div className="App">
          <header className="App-header">
              <div className="info">
                  This is a react app with real-time streaming. Submitting a new profile sends a post request to a
                  Reactive Spring Boot API which adds the profile then notifies the react app about the new profile
                  using a websocket.
              </div>
              <div className="flex-parent">
                  <div className="flex-child">
                      <div className="inner-div">
                          <ProfileForm />
                      </div>
                  </div>
                  <div className="flex-child">
                      <div className="inner-div">
                          <ProfileList />
                      </div>
                  </div>
              </div>
          </header>
        </div>
    );
  }
}

export default App;